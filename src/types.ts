export interface ListItem {
  text: string
  url: string
}

export type Details = {
  title: string,
  items: {
    label: string,
    value: string | string[]
  }[]
}
