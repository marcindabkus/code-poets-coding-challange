import { shallowMount } from '@vue/test-utils'
import VDetails from '@/components/VDetails.vue'

describe('VDetails.vue', () => {
  it('Renders title correctly', () => {
    const wrapper = shallowMount(VDetails, {
      propsData: {
        title: 'Test Title #1'
      }
    })

    expect(wrapper.find('.v-details__name').text()).toContain('Test Title #1')
  })

  it('Renders items list correctly.', () => {
    const wrapper = shallowMount(VDetails, {
      propsData: {
        items: [
          {
            label: 'A simple item',
            value: 'Simple value'
          },
          {
            label: 'A complex item',
            value: [
              'item #1',
              'item #2'
            ]
          }
        ]
      }
    })

    expect(wrapper.findAll('.v-details__items').length).toEqual(2)

    const listItemsWrapper = wrapper.findAll('.v-details__item')
    expect(listItemsWrapper.length).toEqual(2)
    expect(listItemsWrapper.wrappers[0].find('.v-details__item-label').text()).toBe('A simple item')
    expect(listItemsWrapper.wrappers[1].find('.v-details__item-label').text()).toBe('A complex item')
    expect(listItemsWrapper.wrappers[1].findAll('.v-details__item-sublist').length).toEqual(1)
    expect(listItemsWrapper.wrappers[1].findAll('.v-details__item-subitem').length).toEqual(2)
    expect(listItemsWrapper.wrappers[1].findAll('.v-details__item-subitem').wrappers[0].text()).toEqual('item #1')
    expect(listItemsWrapper.wrappers[1].findAll('.v-details__item-subitem').wrappers[1].text()).toEqual('item #2')
  })
})
