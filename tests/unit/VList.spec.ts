import { shallowMount } from '@vue/test-utils'
import VList from '@/components/VList.vue'

const testListItems = [
  {
    text: 'Test text #1',
    url: 'A dummy URL #1'
  },
  {
    text: 'Test text #2',
    url: 'A dummy URL #2'
  }
]

describe('VList.vue', () => {
  it('Renders list items with correct text', () => {
    const wrapper = shallowMount(VList, {
      propsData: {
        items: testListItems
      }
    })

    const listItems = wrapper.findAll('.v-list__item')

    expect(listItems.length).toEqual(2)
    expect(listItems.wrappers[0].text()).toContain('Test text #1')
    expect(listItems.wrappers[1].text()).toContain('Test text #2')
  })

  it('Emits correct event when list item is clicked and passes item URL.', async () => {
    const wrapper = shallowMount(VList, {
      propsData: {
        items: testListItems
      }
    })

    await wrapper.find('.v-list__button').trigger('click')
    expect(wrapper.emitted()['list-item-click']?.[0]).toEqual(['A dummy URL #1'])
  })
})
